Name:          libspatialite
Version:       5.1.0
Release:       1
Summary:       Library intended to extend thee SQLite core
License:       MPL-1.1 OR GPL-2.0-or-later OR LGPL-2.0-or-later
URL:           https://www.gaia-gis.it/fossil/libspatialite
Source0:       https://www.gaia-gis.it/gaia-sins/libspatialite-sources/%{name}-%{version}.tar.gz
# Move private libs to Libs.private in pkg-config file (#1926868)
Patch0:        libspatialite_pkgconfig.patch
# Use pkgconfig to find geos
Patch1:        libspatialite_geos.patch
# Fix incompatibile pointer types
Patch2:        libspatialite_incompat-ptrs.patch

BuildRequires: make gcc
BuildRequires: autoconf automake libtool
BuildRequires: freexl-devel
BuildRequires: geos-devel
BuildRequires: proj-devel
BuildRequires: sqlite-devel
BuildRequires: zlib-devel
BuildRequires: libxml2-devel
BuildRequires: minizip-devel

%description
SpatiaLite is an open source library inteded to extend the SQLite core to
support fully fledged Spatial SQL capabilities.

%package devel
Summary:       Development tools for SpatiaLite
Requires:      %{name} = %{version}-%{release}
Requires: freexl-devel
Requires: geos-devel
Requires: proj-devel
Requires: sqlite-devel
Requires: zlib-devel
Requires: libxml2-devel
Requires: minizip-devel

%description devel
Library files and header files for SpatiaLite development.

%prep
%autosetup -p1

%build
autoreconf -fi
%configure \
    --disable-static \
    --disable-rttopo

%make_build

%install
%make_install
%delete_la

%files
%license COPYING
%doc AUTHORS
%{_libdir}/%{name}.so.8*
%{_libdir}/mod_spatialite.so.8*
%{_libdir}/mod_spatialite.so

%files devel
%doc examples/*.c
%{_includedir}/spatialite.h
%{_includedir}/spatialite
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/spatialite.pc

%changelog
* Mon Jan 20 2025 Funda Wang <fundawang@yeah.net> - 5.1.0-1
- update to 5.1.0

* Mon Jan 24 2022 yaoxin <yaoxin30@huawei.com> - 5.0.0-1
- Upgrade libspatialite to 5.0.0 to solve compilation failure.

* Tue Mar 10 2020 tangjing <tangjing30@huawei.com> - 4.3.0a-10
- Package init
